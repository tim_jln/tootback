> ⚠️ **This script uses your whole archive as a starting block**, including private conversations. The script `filterOutboxToots` filters the posts to keep only the public ones, but make sure your are not uploading any private content.

## What is this?

This is a template to build a static site using your Mastodon Toots archive and allows you to search through your archive.

It was trown together by Julianoë in a few hours. Find it badly styled or coded, imperfect? Feel free to customize it as you want.

## How to use it?

- make sure you have [node](https://nodejs.org/en/download) installed on your OS and [install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) `npm`
- install the dependecies: `npm install`
- download your Mastodon archive from settings/export
- extract outbox.json in the _data folder
- run for local serve : `npx @11ty/eleventy --serve`
- or build static site : `npx @11ty/eleventy`

If you want to host your archive on a subfolder, use [Eleventy's path prefix feature](https://www.11ty.dev/docs/config/#deploy-to-a-subdirectory-with-a-path-prefix).
And add `--pathprefix=YourSubFolder` to your command.

## How is it made?

It uses the static site generator [Eleventy](https://www.11ty.dev/).

[Pagefind](https://github.com/CloudCannon/pagefind/) for that no-config tool that allows to add search to static site very easily

Thanks to Robb Knight for [his writing](https://rknight.me/using-pagefind-with-eleventy-for-search/) on how to integrate 11ty and Pagefind for static search.

## How to improve?

- obviously: styling
- display medias
- display toot data like RT or likes
- could not wrap my head around how to use a function from `.eleventy.js` inside `toot.11tydata.js`, so the `extractStatusID` is duplicated